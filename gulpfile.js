"use strict"

const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync');
const del = require('del');
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify');
const usemin = require('gulp-usemin');
const rev = require('gulp-rev');
const cleanCss = require('gulp-clean-css');
const flatmap = require('gulp-flatmap');
const htmlmin = require('gulp-htmlmin');

gulp
.task('sass', () => { 
    gulp.src('./css/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css'));
})
.task('sass:watch', function() {
    gulp.watch('./css/*.scss', ['sass']);
})
.task('browser-sync', () => {
    const files = ['./*.html','./css/*.css','./img/*.{png, jpeg, gif}', './js/*.js']
    browserSync.init(files, {
        server: {
            baseDir: './'
        }
    })
})
.task('default', ['browser-sync'], function(){
    gulp.start('sass:watch');
})
.task('clean', () => {
    return del(['dist'])
})
.task('copyfont', () => {
    gulp
    .src('./node_modules/open-iconic/font/font/*.{ttf,woff,eof,svg,eot,otg}*')
    .pipe(gulp.dest('./dist/fonts'));
})
.task('imagemih', () => {
    return gulp
        .src('./img/*.{.png, jpeg, jpg, gif}')
        .pip(imagemin({optimizationLevel: 3, progresive: true, interlaced: true}))
        .pipe(gulp.dest('dist/images'))
})
.task('usemin', () => {
    return gulp.src('./*,html')
        .pipe(flatmap((stream, file) =>{
            return stream
                    .pipe(usemin({
                        css:[rev()],
                        html:[() => htmlmin({collapseWhitespace: true})],
                        js: [uglify(), rev()],
                        inlinejs: [uglify()],
                        inlinecss: [cleanCss(), 'concat']
                    }));
                }))
                .pipe(gulp.dest('dist/'))
})

.task('build', ['clean'], () => {
    gulp.start('imagemin', 'usemin')
});
